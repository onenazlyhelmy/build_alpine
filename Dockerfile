FROM onenazlyhelmy/alpine-openssh-client


#RUN chmod +x /usr/bin/helm
RUN apk update && apk add git && apk add curl && apk add iputils && apk add sshpass
RUN apk add py-pip && apk add gcc musl-dev python3-dev libffi-dev openssl-dev cargo make
RUN python3 -m pip install --upgrade pip
#RUN python3 -m pip install azure-cli
CMD sh
#ENV http_proxy 'http://user:pass@10.78.2.60:9090'
#ENV https_proxy 'http://user:pass@10.78.2.60:9090'

RUN mkdir /apps
RUN chmod 777 /apps
WORKDIR /apps

#ENTRYPOINT ["helm"]
#CMD ["--help"]
